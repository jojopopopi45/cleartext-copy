---
title: Encrypted E-mail with Cleartext Copies
docname: draft-dkg-mail-cleartext-copy-02
category: std
ipr: trust200902
consensus: yes
area: sec
workgroup: Network Working Group
keyword: Internet-Draft
stand_alone: yes
submissionType: IETF
venue:
  group: "Secdispatch"
  type: "Working Group"
  mail: "secdispatch@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/secdispatch/"
  repo: "https://gitlab.com/dkg/cleartext-copy"
  latest: "https://dkg.gitlab.io/cleartext-copy/"
author:
  -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    email: dkg@fifthhorseman.net
--- abstract

When an e-mail program generates an encrypted message to multiple recipients, it is possible that it has no encryption capability for at least one of the recipients.

In this circumstance, an e-mail program may choose to send the message in cleartext to the recipient it cannot encrypt to.

This draft currently offers several possible approaches when such a choice is made by the sender, so that the recipient can reason about and act on the cryptographic status of the message responsibly.

--- middle

# Introduction

This document is concerned with end-to-end encrypted e-mail messages, regardless of the type of encryption used.
Both S/MIME ({{?RFC8551}} and PGP/MIME ({{?RFC3156}}) standards support the creation and consumption of end-to-end encrypted e-mail messages.

The goal of this document is to enable a receiving MUA to have solid, reliable behavior and security indicators based on the status of any particular received message, in particular when the sender of the message may have emitted a cleartext copy.

The document currently does not pick a single mechanism, as it is more of a survey/problem statement.

The final document should select at most a single mechanism with a clear default behavior.

{::boilerplate bcp14-tagged}

## Terminology

In this draft:

- MUA is short for Mail User Agent; an e-mail client.

# Scenarios

The following scenarios are examples where an encrypted message might be produced but some copies of the message are sent or stored in the clear.
In all scenarios, Alice is composing a new message to Bob and at least one other copy is generated.
Alice has a cryptographic key for Bob, and knows that Bob is capable of decrypting e-mail messages.

In each of the following scenarios, Alice's MUA generates an e-mail, and knows that there is at least one cleartext copy of the message stored on a system not under Alice's personal control.

## Simple Three-Party E-mail {#simple-three-party}

Alice sends a message to Bob and Carol, but Alice has no encryption-capable key for Carol.
She encrypts the copy to Bob, but sends a cleartext copy to Carol.

## Cleartext Remote Drafts Folder {#drafts}

Alice's MUA stores all draft copies of any message she writes in the clear in a Drafts folder, and that folder is itself stored on an IMAP server.
When she composes a message, the IMAP server has a cleartext copy of the draft, up until and including when she clicks "Send".
Her MUA instructs the IMAP server to delete the draft version of the message, but it also knows that it had at one point a cleartext copy, and cleartext might persist forever.
  
## Cleartext Remote Sent Folder {#sent}

Unlike in {{drafts}}, copies of messages sent to Alice's draft folder are encrypted or only stored locally.
But when sending an e-mail message to Bob, her MUA generates a cleartext copy an places it in her Sent folder, which is also stored on IMAP.

## Public Mailing List {#mailing-list}

Alice "Replies All" to message from Bob on a public mailing list.
The public mailing list has on encryption-capable public key (it is archived publicly in the clear), so Alice cannot encrypt to it.
But Alice's MUA is configured to opportunistically encrypt every copy possible, so the copy to Bob is encrypted.

## Trusted Mailserver {#trusted-mailserver}

Alice and Bob work in different organizations, and Alice's MUA has a policy of encrypting to outside peers that does not apply to members of her own organization.
Alice's co-worker David is in Cc on the message, and Alice and David both share a trusted mail server, so Alice does not feel the need to encrypt to Carol.
But she wants to defend against the possibility that Bob's mail server could read the contents of her message.
  
# Problems

Receiving MUA often needs to behave differently when handling a message with a different cryptographic status.

## Reply All

The most visible responsibility of an MUA that receives an encrypted message is to avoid leaking the contents of the message in the clear during a reply (see {{I-D.ietf-lamps-e2e-mail-guidance}}).

In scenarios where a recipient of the message cannot receive encrypted mail (e.g. the public mailing list example in {{mailing-list}}), a "Reply all" message is unlikely to be an act of effective communication.
In the example from that section, if Bob receives an encrypted copy of Alice's message, and he also chooses to "Reply All" to it, his MUA will either:

- generate a cleartext copy to the mailing list, thereby leaking the contents of what appears to be an encrypted message, or
- send the mailing list a message encrypted only to Alice, which the rest of the mailing list cannot read.

Neither outcome is satisfactory.

## User Models

User experience of end-to-end encrypted e-mail is notoriously poor.
A system that just silently encrypts as aggressively as possible might well produce more messages that are unreadable to any intervening transport agents.
And, done right, it might even avoid creating messages that are unreadable by the intended recipient.

However, such an opportunistic model is not the end goal of a system of end-to-end encryption.
An end-to-end encrypted system typically involves a some sort of user expectation (see {{Section 4 of I-D.knodel-e2ee-definition}}).
In a legacy system like e-mail, where many messages will not be end-to-end encrypted, satisfying the user expectation requires that the user have a clear understanding of what specifically is end-to-end encrypted.

When a user receives an encrypted message that is also posted in the clear to a publicly-visible archive (as in {{mailing-list}}), that violates most user expectations of end-to-end encryption.

# Solutions

This document has not yet reached consensus on what the right solution is.
Two major classes of possible solution are:

- A message that was generated with a cleartext copy can explicitly indicate that such a cleartext copy exists, which allows the recipient to reason about it differently than a normal end-to-end encrypted message.
- Forbid MUAs from generating a cleartext copy.

## Explicit Indicator of Cleartext Copy {#explicit-indicator}

It seems plausible that a MUA generating an end-to-end encrypted e-mail message with a cleartext copy could indicate to its recipients that a cleartext copy was also generated.

Each recipient could then reason about it differently, as compared to an end-to-end-encrypted e-mail message without a cleartext copy.

For example, a recipient doing "reply all" to such an encrypted message could take a different strategy and permit re-sending cleartext copies.
For more discussion about how to use such an indicator, see {{handling-explicit-indicator}}.

The proposals here use e-mail headers, so see also {{protected-headers}} for security considerations.

### Cleartext-Copy Header Field {#cleartext-copy}

This document could specify a new e-mail header field with the name `Cleartext-Copy`.
The only defined values of this field are `0` and `1`.

A MUA that creates an encrypted message with a cleartext copy MUST add this header field with a value of `1` to each encrypted copy of the message.

By default, any end-to-end encrypted message that does not have this header field is presumed to have the field with a value of `0` -- meaning that no cleartext copies made by the sending MUA.

FIXME: what if the default was `1` instead of defaulting to `0`?

### Cleartext-Copy-To Header Field {#cleartext-copy-to}

This document could specify a new e-mail header field with the name `Cleartext-Copy-To`.

This header field's value is defined to be an `address-list`, as specified in {{!RFC5322}}.

A MUA that creates an encrypted message with a cleartext copy to any recipient MUST add this header field to each encrypted copy of the message.
The contents of the field are the list of e-mail addresses that were sent cleartext copies of the message.

By default, any end-to-end encrypted message that does not have this header field, or that has it with empty contents, is presumed to have no cleartext copies made by the sending MUA.

FIXME: it is unclear how a MUA that uses a cleartext remote Drafts (see {{drafts}}) or Sent (see {{sent}}) folder should populate this field to indicate to the recipient that a cleartext copy was sent to the IMAP server.

### Encrypted-To Header Field

This document could specify a new e-mail header field with the name `Encrypted-To`.

This header field's value is defined to be an `address-list`, as specified in {{!RFC5322}}.

A MUA that creates any encrypted message includes the full `address-list` of all recipients in `To` or `Cc` that it was able to successfully encrypt the message to.

The recipient of an encrypted message can then infer based on the contents of this header whether an additional cleartext copy was generated.

FIXME: it is unclear how a MUA that uses a cleartext remote Drafts (see {{drafts}}) or Sent (see {{sent}}) folder should populate this field to indicate to the recipient that a cleartext copy was sent to the IMAP server.

FIXME: if the recipient receives an encrypted copy of a message without this header in it, they know that the sender does not support this mechanism.
What should be the default assumption in that case: cleartext copy or no cleartext copy?

## Forbid Cleartext Copy

Another approach would simply be to declare that a MUA that generates an end-to-end encrypted e-mail message MUST NOT store or transmit a cleartext copy.

## Handling an Encrypted Message with a Cleartext Copy {#handling-explicit-indicator}

When one of the copies of the message is known to be sent or stored in clear, a MUA might treat "Reply All" differently.

For example, it might be willing to send an additional cleartext copy to some of the recipients.

FIXME: what other behaviors might need changing?

# Picking a Solution

This draft currently does not choose a specific solution, but it should not be published as a final document without choosing at most one solution.
Factors to consider when choosing a solution among those presented include:

- complexity of implementation for senders
- complexity of implementation for receivers
- additional information leakage
- risk of user confusion (complexity of user mental models)

# User Experience Considerations

As noted in {{?I-D.ietf-lamps-e2e-mail-guidance}}, representing the cryptographic status of a message is challenging even under good circumstances.

This is because storing a cleartext copy with a third party breaks most expectations of "end-to-end encryption" (see {{?I-D.knodel-e2ee-definition}}).

When a single message has multiple cryptographic statuses depending on which copy of the message is being examined, it is even more challenging to represent the cryptographic status of any particular copy of the message.

Aside from changing its behavior around Reply All, how should an MUA treat such a message?

# IANA Considerations

For example, if we go with {{cleartext-copy}}:

The IANA registry of [Message Headers](https://www.iana.org/assignments/message-headers/message-headers.xhtml) should be updated to add a row with Header Field Name `Cleartext-Copy` , no template, protocol `mail`, status `standard`, and a reference to this document.

# Security Considerations

## Misrepresentations By Sender are Out of Scope {#misrepresentations}

This document describes security considerations between mutually-cooperating, end-to-end encryption-capable MUAs.
Either party could of course leak cleartext contents of any such message either deliberately or by accident.

In some cases, such as a `Bcc` scenario, the sending MUA is deliberately taking action on the sender's behalf that they do not want the (listed) recipient to know about.
Indicating to the listed recipient that a `Bcc`ed copy was emitted in the clear may violate the sender's expectations about what was done with the message.

This specification is not intended to detect fraud, misbehavior, or deliberate misrepresenation from one of the clients.

## Cryptographic Guarantees {#protected-headers}

For the proposed solutions that require a header field, that header field itself needs cryptographic protections, or an intervening mail transport agent could inject it to tamper with the apparent cryptographic status of the message.

For this reason, any header field involved in this must be provided with header protection, as described in {{?I-D.ietf-lamps-header-protection}}.

Additionally, since this is dealing with encrypted messages only, any relevant header field should probably be stripped from the message before sending, to avoid indicating to a mail transport agent that some cleartext copy of the message is available somewhere.

--- back

# Acknowledgements

The author would like to thank Daniel Huigens and Bart Butler for explaining the circumstances behind this situation.

# Document History

## Changes from draft-dkg-mail-cleartext-copy-00 to draft-dkg-mail-cleartext-copy-01

Added some discussion about user expectations.
